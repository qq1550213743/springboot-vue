package com.xyj;

import com.xyj.repository.BookRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class BookSpringbootApplicationTests {

    @Autowired
    private BookRepository bookRepository;
    @Test
    void contextLoads() {
        System.out.println(bookRepository.findAll());
    }

}
