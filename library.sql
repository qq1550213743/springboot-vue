/*
 Navicat MySQL Data Transfer

 Source Server         : 新连接
 Source Server Type    : MySQL
 Source Server Version : 50515
 Source Host           : localhost:3306
 Source Schema         : library

 Target Server Type    : MySQL
 Target Server Version : 50515
 File Encoding         : 65001

 Date: 15/07/2020 11:50:54
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for book
-- ----------------------------
DROP TABLE IF EXISTS `book`;
CREATE TABLE `book`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `author` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `publish` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pages` int(10) NULL DEFAULT NULL,
  `price` float(10, 2) NULL DEFAULT NULL,
  `bookcaseid` int(10) NULL DEFAULT NULL,
  `abled` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of book
-- ----------------------------
INSERT INTO `book` VALUES (1, '上下五千年', '曹操', '中国人民出版社', 999, 99.99, 10, 1);
INSERT INTO `book` VALUES (2, 'SPSS统计', '王五', 'SPSS出版社', 330, 26.00, 1, 1);
INSERT INTO `book` VALUES (3, '四级必备词汇', '李四', '清华出版社', 150, 17.30, 1, 1);
INSERT INTO `book` VALUES (4, '六级必备词汇', '张三', '北大出版社', 220, 59.00, 3, 1);
INSERT INTO `book` VALUES (5, '高考必备', '王强', '上海出版公司', 300, 27.30, 4, 1);
INSERT INTO `book` VALUES (7, '123', '123', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `book` VALUES (8, '123', '365', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `book` VALUES (9, 'qqqq', 'qqq', NULL, NULL, NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
